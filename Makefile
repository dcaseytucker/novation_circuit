SOURCES=novation.c
CC=gcc
CFLAGS=-I.

.PHONY: run

%.o: %.c
	$(CC) -o $@ $< $(CFLAGS)

all: novation

run:
	./novation
