#include "novation.h"
#include <stdio.h>

void print_u7(u7 u)
{
	printf("%d\n", u.value);
}

void print_s7(s7 s)
{
	if( s.sign == 1 )
	{
		if( s.value == 0 )
			printf(" %d", s.value);
		else
			printf("+%d", s.value);
	}
	else
	{
		printf("-%d", s.value);
	}
	printf("\n");
}

int main(int argc, char **argv)
{
	FILE *f = fopen("all patches.syx","rb");
	int read = fread(&mypatch, 350, 1, f);
	fclose(f);

	printf("%s\n", mypatch.patch.name);
	//printf("Osc1 Waveform  "); print_u7( mypatch.patch.osc1.waveform );
	printf("Osc1 Pitchbend "); print_s7( mypatch.patch.osc1.pitchbend );
	printf("Osc1 Pulsewidth"); print_s7( mypatch.patch.osc1.pulsewidth);
	return 0 ;
}
