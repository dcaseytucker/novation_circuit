/*
typedef union {
	struct {
		char value :6,
			sign  :1,
			n     :1;
	};
	signed char c;
} s7;

typedef union {
	struct {
		char value :7;
		char n     :1;
	};
	signed char c;
} u7;
*/
typedef unsigned char u7;
typedef signed char s7;
typedef unsigned char b7;

typedef struct d7 {
	u7 value; // 0 to 357 in steps of 3 degrees
} d7;

typedef struct lfo_sync {
	u7 value;
} lfo_sync;

typedef struct drive_type {
	u7 value;
} drive_type;


typedef enum {
	monophonic,
	monophonic_ag,
	polyphonic
} polymode_enum;

typedef enum {
	// waveforms
	sine,
	triangle,
	sawtooth,
	saw_9_1_pw,
	saw_8_2_pw,
	saw_7_3_pw,
	saw_6_4_pw,
	saw_5_5_pw,
	saw_4_6_pw,
	saw_3_7_pw,
	saw_2_8_pw,
	saw_1_9_pw,
	pulse_width,
	square,
	// wavetables
	sine_table,
	analogue_pulse,
	analogue_sync,
	triangle_saw_blend,
	digital_nasty_1,
	digital_nasty_2,
	digital_saw_square,
	digital_vocal_1,
	digital_vocal_2,
	digital_vocal_3,
	digital_vocal_4,
	digital_vocal_5,
	digital_vocal_6,
	random_collection_1,
	random_collection_2,
	random_collection_3
} oscwave_enum;

typedef enum {
	normal,
	osc1_bypass,
	osc1_osc2_bypass
} filter_routing_enum;

typedef enum {
	low_pass_12,
	low_pass_24,
	band_pass_6,
	band_pass_12,
	high_pass_12,
	high_pass_24
} filter_type_enum;

typedef enum {
	diode,
	valve,
	clipper,
	cross_over,
	rectifier,
	bit_reducer,
	rate_reducer
} drive_type_enum;

typedef enum {
	lfo_sine,
	lfo_triangle,
	lfo_sawtooth,
	lfo_square,
	lfo_random_s_h,
	lfo_time_s_h,
	lfo_piano_envelope,
	lfo_sequence_1,
	lfo_sequence_2,
	lfo_sequence_3,
	lfo_sequence_4,
	lfo_sequence_5,
	lfo_sequence_6,
	lfo_sequence_7,
	lfo_alternative_1,
	lfo_alternative_2,
	lfo_alternative_3,
	lfo_alternative_4,
	lfo_alternative_5,
	lfo_alternative_6,
	lfo_alternative_7,
	lfo_alternative_8,
	lfo_chromatic,
	lfo_chromatic_16,
	lfo_major,
	lfo_major_7,
	lfo_minor_7,
	lfo_min_arp_1,
	lfo_min_arp_2,
	lfo_diminished,
	lfo_dec_minor,
	lfo_minor_3rd,
	lfo_pedal,
	lfo_4ths,
	lfo_4ths_x12,
	lfo_maj_1625,
	lfo_min_1625,
	lfo_2511
} lfowave_enum;

typedef enum {
	fade_in,
	fade_out,
	gate_in,
	gate_out
} lfo_fade_mode_enum;

typedef enum {
	phaser,
	chorus
} chorus_type_enum;

typedef enum {
	// not at all sure about this enum
	sync_off,
	sync_32,
	sync_32_dot,
	sync_16,
	sync_16_dot,
	sync_8,
	sync_8_dot,
	sync_4,
	sync_4_dot,
	sync_2,
	sync_2_dot,
	sync_whole,
	sync_whole_dot,
	sync_bar_2,
	sync_bar_3,
	sync_bar_4,
	sync_bar_5,
	sync_bar_6,
	sync_bar_7,
	sync_bar_8,
	sync_bar_9,
	sync_bar_10,
	sync_bar_11,
	sync_bar_12,
	sync_bar_13,
	sync_bar_14,
	sync_bar_15,
	sync_bar_16,
} lfo_sync_enum;

typedef enum {
	off,
	portrate,
	post_fx,
	osc1_interpol,
	osc1_pulewidth,
	osc1_vsync_depth,
	osc1_density,
	osc1_density_detune,
	osc1_semitones,
	osc1_cents,
	osc2_interpol,
	osc2_pulewidth,
	osc2_vsync_depth,
	osc2_density,
	osc2_density_detune,
	osc2_semitones,
	osc2_cents,
	mixer_osc1,
	mixer_osc2,
	mixer_ringmod,
	mixer_noise,
	filter_frequency,
	filter_resonance,
	filter_drive,
	filter_tracking,
	filter_env2_to_frequency,
	env_amp_a,
	env_amp_d,
	env_amp_s,
	env_amp_r,
	env_filt_a,
	env_filt_d,
	env_filt_s,
	env_filt_r,
	env_3_delay,
	env_3_a,
	env_3_d,
	env_3_s,
	env_3_r,
	lfo1_rate,
	lfo1_rate_sync,
	lfo1_slew,
	lfo2_rate,
	lfo2_rate_sync,
	lfo2_slew,
	distortion,
	chorus_level,
	chorus_rate,
	chorus_feedback,
	chorus_mod_depth,
	chorus_delay,
	matrix_1_depth,
	matrix_2_depth,
	matrix_3_depth,
	matrix_4_depth,
	matrix_5_depth,
	matrix_6_depth,
	matrix_7_depth,
	matrix_8_depth,
	matrix_9_depth,
	matrix_10_depth,
	matrix_11_depth,
	matrix_12_depth,
	matrix_13_depth,
	matrix_14_depth,
	matrix_15_depth,
	matrix_16_depth,
	matrix_17_depth,
	matrix_18_depth,
	matrix_19_depth,
	matrix_20_depth,
} macro_destination_enum;

typedef enum {
	direct,
	mod_wheel,
	after_touch,
	expression,
	velocity,
	keyboard,
	lfo1_plus,
	lfo1_plus_minus,
	lfo2_plus,
	lfo2_plus_minus,
	env_amp,
	env_filter,
	env_3
} mod_source_enum;

typedef enum {
	to_none,
	to_osc1_pitch,
	to_osc2_pitch,
	to_osc1_vsync,
	to_osc2_vsync,
	to_osc1_pulsewidth,
	to_osc2_pulsewidth,
	to_mixer_osc1,
	to_mixer_osc2,
	to_mixer_noise,
	to_mixer_ringmod,
	to_drive_amount,
	to_frequency,
	to_resonance,
	to_lfo1_rate,
	to_lfo2_rate,
	to_amp_env_decay,
	to_mod_env_decay
} mod_destination_enum;

typedef enum {
	chamber,
	small_room,
	large_room,
	small_hall,
	large_hall,
	great_hall
} reverb_type_enum;

// session control

typedef enum {
	ratio_1_1,
	ratio_4_3,
	ratio_3_4,
	ratio_3_2,
	ratio_2_3,
	ratio_2_1,
	ratio_1_2,
	ratio_3_1,
	ratio_1_3,
	ratio_4_1,
	ratio_1_4,
	ratio_1_OFF,
	ratio_OFF_1
} delay_lr_ratio_enum;

typedef enum {
	drum_1,
	drum_2,
	drum_3,
	drum_4,
	sidechain_off
} sidechain_source_enum;

typedef struct {
	//f0 00 20 29 01 60 01 00 00
	//f0 00 20 29 01 60 01 01 00
	//f0 00 20 29 01 60 01 02 00
	char start;      // f0
	unsigned char mfid[3];    // 00 20 29
	unsigned char device_id;  // 01
	unsigned char model_id;   // 60
	unsigned char message_id; // 01
	
	union {
		struct patch {
			u7 slot;     // 01
			u7 n_0;      // 00
			char name[16]; // "Bass-ic Square  "
			struct category { u7 value; } category; // 02
			struct genre    { u7 value; } genre;    // 03
			char a[14];    // 00 00 00 00 00 00 00 00 00 00 00 00 00 00
			struct polymode { u7 value; } polymode; // 01
			u7 portrate; // 4d
			s7 preglide; // 40
			s7 octave;   // 3e
			struct osc {
				struct oscwave { u7 value; } waveform; // 0c
				u7 interpol;   // 7f
				s7 pulsewidth; // 40
				u7 vsync_depth;// 00
				u7 density;    // 00
				u7 density_detune; // 43
				s7 semitones;  // 34
				s7 cents;      // 40
				s7 pitchbend;  // 4c
			} osc1, osc2;
			struct mixer {
				u7 osc1; // 6a
				u7 osc2; // 6b
				u7 ringmod; // 60
				u7 noise;   // 00
				s7 pre_fx;  // 43
				s7 post_fx; // 44
			} mixer;
			struct filter {
				struct filter_routing { u7 value; } routing;
				u7 drive;
				drive_type drive_type;
				struct filter_type { u7 value; } type;
				u7 frequency;
				u7 tracking;
				u7 resonance;
				u7 q_normalize;
				u7 env_2_to_frequency;
			} filter;
			struct env  { u7 velocity,attack,decay,sustain,release; } env_amp, env_filt;
			struct env3 { u7 delay   ,attack,decay,sustain,release; } env3;
			struct lfo {
				struct lfowave { u7 value; } waveform;
				d7 phase_offset;
				u7 slew;
				u7 delay;
				lfo_sync delay_sync;
				u7 rate;
				lfo_sync rate_sync;
				b7 one_shot;
				b7 key_sync;
				b7 common_sync;
				b7 delay_trigger;
				struct fade_mode { u7 value; } fade_mode;
			} lfo1, lfo2;
			struct fx {
				u7 distortion_level;
				u7 chorus_level;
				struct eq {
					u7 frequency;
					s7 level;
				} eq_bass, eq_mid, eq_treble;
				drive_type distortion_type;
				u7 distortion_compensation;
				struct chorus_type { u7 value; } chorus_type;
				u7 chorus_rate;
				struct rate_sync { u7 value; } chorus_rate_sync;
				u7 chorus_feedback;
				u7 chorus_mod_depth;
				u7 chorus_delay;
			} fx;
			struct mod {
				struct mod_source { u7 value; } source_1, source_2;
				s7 depth;
				struct mod_destination { u7 value; } destination;
			} mod_matrix[20];
			struct macro {
				u7 position;
				struct dest {
					struct macro_destination { u7 value; } destination;
					u7 start;
					u7 end;
					s7 depth;
				} a,b,c,d;
			} macro_knob[8];
		} patch;
		unsigned char data[342];
	};

	char terminator; // f7
} circuit_patch;

/*
f000 2029 0160 0100 0042 6173 732d 6963  .. ).`...Bass-ic
2053 7175 6172 6520 2002 0300 0000 0000   Square  .......
0000 0000 0000 0000 0001 4d40 3e0c 7f40  ..........M@>..@
0000 4334 404c 017f 4000 0000 4040 4c6a  ..C4@L..@...@@Lj
6b60 0043 4400 0000 0123 7f39 4040 4002  k`.CD....#.9@@@.
5a60 4840 0000 0000 000a 4640 2806 0000  Z`H@......F@(...
0000 4b10 0200 0000 0000 4400 0000 0000  ..K.......D.....
0000 4040 4040 7d40 0000 0000 0006 6401  ..@@@@}@......d.
1400 4040 2200 0040 0006 0040 0c00 0040  ..@@"..@...@...@
0e00 0040 0000 0040 0000 0040 0000 0040  ...@...@...@...@
0000 0040 0000 0040 0000 0040 0000 0040  ...@...@...@...@
0000 0040 0000 0040 0000 0040 0000 0040  ...@...@...@...@
0000 0040 0000 0040 0000 0040 0000 0040  ...@...@...@...@
0000 0040 0000 0900 7f51 1000 7f30 0000  ...@.....Q...0..
7f40 0000 7f40 4015 007f 6f00 007f 4000  .@...@@...o...@.
007f 4000 007f 4000 1900 3e7f 1f00 3e66  ..@...@...>...>f
1e41 7f6c 0000 7f40 0016 007f 5e00 007f  .A.l...@....^...
4000 007f 4000 007f 4000 0f2d 584c 0000  @...@...@..-XL..
7f40 0000 7f40 0000 7f40 0034 0017 6c35  .@...@...@.4..l5
007f 4b00 007f 4000 007f 4000 1400 7f5d  ..K...@...@....]
0000 7f40 0000 7f40 0000 7f40 002e 007f  ...@...@...@....
7f30 007f 7900 007f 4000 007f 40f7       .0..y...@...@.
*/
