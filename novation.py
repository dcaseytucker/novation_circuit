from cffi import FFI
ffi = FFI()

with open('novation.h','r') as h:
	ffi.cdef("\n"+h.read())
	(typedefs, structs, unions) = ffi.list_types()


class Patch(object):
	def __init__(self, data):
		patch_struct = ffi.new("circuit_patch*")
		patch_buffer = ffi.buffer(patch_struct)
		patch_buffer[:] = data
		self.struct = patch_struct[0]

	def __str__(self):
		py = convert_to_python(self.struct.patch)
		#s = ffi.sizeof(self.struct.patch)
		return str(dict(py))



def struct_to_enum(x):
	t = ffi.typeof(x)
	return enum(t.cname.replace('struct ','')+'_enum', x.value)

def struct_to_degrees(s):
	return s.value * 3;

def __convert_struct_field( s, fields ):
	for field,fieldtype in fields:
		if fieldtype.type.cname == 'signed char':
			yield (field,s7(getattr(s,field)))
		elif fieldtype.type.kind == 'primitive':
			yield (field,getattr( s, field ))
		else:
			yield (field, convert_to_python( getattr( s, field ) ))

def convert_to_python(s):
	type=ffi.typeof(s)
	if type.kind == 'struct':
		struct_name = type.cname.replace('struct ','')
		if struct_name == 'd7':
			return struct_to_degrees(s)
		elif struct_name in structs and struct_name+"_enum" in typedefs:
			return struct_to_enum(s)
		return dict(__convert_struct_field( s, type.fields ) )
	elif type.kind == 'array':
		if type.item.kind == 'primitive':
			if type.item.cname == 'char':
				return ffi.string(s)
			else:
				return [ s[i] for i in range(type.length) ]
		else:
			return [ convert_to_python(s[i]) for i in range(type.length) ]
	elif type.kind == 'primitive':
		return int(s)

def enum(enum, value):
	return ffi.string( ffi.cast(enum, value) )

def s7(value):
	return int(value) - 64

with open('all patches.syx','rb') as f:
	data = f.read()
	patch = Patch(data[:350])
	print patch
	#print enum("oscwave", struct.patch.osc1.waveform.value)
	#print enum("lfowave", struct.patch.lfo1.waveform.value)
	#print s7(struct.patch.osc1.pitchbend)
	#patch = convert_to_python(struct[0])
	#print patch
	#print ffi.list_types()
